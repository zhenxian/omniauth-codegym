# Omniauth::Codegym

CodeGym用のカスタムStrategyで、OmniAuthのプラグインとして使える。
GitLabはOmniAuthを組み込んでいるのでCodeGym用のOAuthが使えるようになる。

## 組み込み

Railsに組み込むには、プロジェクトを作ってGemfileに次の記述を加えて`bundle install`する。

```ruby
gem 'omniauth-codegym', :git => 'git@gitlab.com:libtech/omniauth-codegym.git'
```

開発中はローカルのgitを見に行くと良い。

```ruby
gem 'omniauth-codegym', :git => '/Users/adam/Documents/workspace/omniauth-codegym'
```

## GitLab

Debian8をDockerで起動し、GitLabをインストールする。

https://about.gitlab.com/downloads/#debian8

reconfigureで止まるときはこれを実行する。

https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/doc/common_installation_problems/README.md#reconfigure-freezes-at-ruby_blocksupervise_redis_sleep-action-run

```
initctl restart gitlab-runsvdir
```

/etc/gitlab/gitlab.rbを編集し下記を加える。

```
- {'name':'codegym', 'app_id':'gitlab', 'app_secret':'secret'}
```

Gemfileに追加したあと`bundle install`するとエラーになるので、まずはapt-getから始める。
```
sudo apt-get update
sudo apt-get install -y git ruby ruby2.1-dev make gcc libicu-dev zlib1g-dev g++ cmake pkg-config libmysqld-dev libpq-dev libkrb5-dev
```

## 参考

[Gemの作り方](http://masarakki.github.io/blog/2014/02/15/how-to-create-gem/)
[omniauth-facebook](https://github.com/mkdynamic/omniauth-facebook)
[Gemfileでのgem指定](http://qiita.com/yaotti/items/510779877e515a1155db)
[GitLabで必要な要素](https://gitlab.com/gitlab-org/gitlab-ce/blob/7acea6bde9dbffafc99401eb0d7bb748c1f06d3f/lib/gitlab/o_auth/user.rb)